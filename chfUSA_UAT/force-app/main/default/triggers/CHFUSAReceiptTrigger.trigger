trigger CHFUSAReceiptTrigger on Opportunity(before update, after update, before insert) { 
    public Contact contactPerosn = new Contact();
    String [] emailAddress = new List<String>();
    Opportunity opty = new Opportunity();   
    Integer fY;
    if(Trigger.isUpdate){
        if(Trigger.isAfter){        
            try{ 
                for(Opportunity oppObj : Trigger.new) {    
                    fY = Integer.valueof(oppObj.Oppty_USA_Financial_year__c);
                    if(String.isNotBlank(oppObj.Transaction_ID__c) && String.isNotBlank(oppObj.Mode_of_Payment__c) && oppObj.Amount > 0 && oppObj.StageName.equals('Payment Received') && oppObj.EmailTriggered__c == True && oppObj.Suppress_Receipt__c == false ){    
                        if(oppObj.Donor__c != Null){
                            if(oppObj.Donor__c != oppObj.npsp__Primary_Contact__c){
                                Contact primaryContact = [SELECT Id,Name,FirstName,LastName,Email FROM Contact WHERE Id=: oppObj.npsp__Primary_Contact__c limit 1];
                                Contact donorContact = [SELECT Id,Name,FirstName,LastName,Email FROM Contact WHERE Id=: oppObj.Donor__c limit 1];
                                emailAddress.add(primaryContact.Email);
                                //SendMail.sendPDF(primaryContact.Id,fY,primaryContact.FirstName,primaryContact.LastName,oppObj.id,emailAddress);
                                emailAddress.add(donorContact.Email);
                                SendMail.sendPDF(donorContact.Id,fY,donorContact.FirstName,donorContact.LastName,oppObj.id,emailAddress);
                            }else if(oppObj.Donor__c == oppObj.npsp__Primary_Contact__c){
                                contactPerosn = [SELECT Id,Name,FirstName,LastName,Email FROM Contact WHERE Id=: oppObj.npsp__Primary_Contact__c limit 1];   
                                emailAddress.add(contactPerosn.Email);
                                SendMail.sendPDF(contactPerosn.Id,fY,contactPerosn.FirstName,contactPerosn.LastName,oppObj.id,emailAddress);
                            }
                        }else{
                            contactPerosn = [SELECT Id,Name,FirstName,LastName,Email FROM Contact WHERE Id=: oppObj.npsp__Primary_Contact__c limit 1];   
                            emailAddress.add(contactPerosn.Email);
                            SendMail.sendPDF(contactPerosn.Id,fY,contactPerosn.FirstName,contactPerosn.LastName,oppObj.id,emailAddress);
                            
                        }
                        opty = [Select Id, EmailTriggered__c from Opportunity where Id =: oppObj.Id ];
                        opty.EmailTriggered__c = False;
                        update opty;
                    }
                }
            }catch(Exception ex){
                System.Debug('ex'+ex);        
            }    
        }
    }
}