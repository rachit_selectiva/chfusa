trigger RestrictDonationSummary on DonationSummary__c (before update, before delete, after insert, after update) {    
    
    Id profileId= userinfo.getProfileId();
    String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
    Donation__c don = Donation__c.getInstance(profileId);
    
    if(profileName == 'Integration User'){
        don.Suppress_Insert_Validation__c = false;
    }
    if(profileName == 'System Administrator'){
        don.Suppress_Insert_Validation__c = true;
    }
    if(Trigger.isDelete){
        for(DonationSummary__c olddonsum : Trigger.old){
            if([SELECT ID, StageName, Amount FROM Opportunity Where ID = :olddonsum.Opportunity__c].size()>0){
                Opportunity opptyRec = [SELECT ID, StageName, Amount,Suppress_Receipt__c FROM Opportunity Where ID = :olddonsum.Opportunity__c];
                if(opptyRec.StageName == 'Payment Received'){
                    if(opptyRec.Suppress_Receipt__c == false){
                        olddonsum.addError('Please enable Suppress Receipt checkbox to perform modifications.');
                    }else{
                        opptyRec.EmailTriggered__c = false;   
                    }
                }else{
                    opptyRec.Amount = opptyRec.Amount - olddonsum.Total__c;
                    try{
                        Update opptyRec;  
                    }catch(Exception ex){
                        olddonsum.addError(ex);
                    }                
                }
            }       
        } 
    }
    if(Trigger.isUpdate){  
        for(DonationSummary__c newdonsum : Trigger.new){
            Opportunity opptyRec = [SELECT ID, StageName, Amount ,Suppress_Receipt__c,EmailTriggered__c FROM Opportunity Where ID = :newdonsum.Opportunity__c];
            if(opptyRec.StageName == 'Payment Received'){
                if(opptyRec.Suppress_Receipt__c == false){
                    newdonsum.addError('Please enable Suppress Receipt checkbox to perform modifications.');
                }else{
                    opptyRec.EmailTriggered__c = false;   
                }
            }
        }          
    }  
    if(don.Suppress_Insert_Validation__c){
        if(Trigger.isInsert){  
            for(DonationSummary__c newdonsum : Trigger.new){
                Opportunity opptyRec = [SELECT ID, StageName, Amount ,Suppress_Receipt__c,EmailTriggered__c FROM Opportunity Where ID = :newdonsum.Opportunity__c];
                if(opptyRec.StageName == 'Payment Received'){
                    if(opptyRec.Suppress_Receipt__c == false){
                        newdonsum.addError('Please enable Suppress Receipt checkbox to perform modifications.');
                    }else{
                        opptyRec.EmailTriggered__c = false;   
                    }
                }
            }          
        } 
    }
    if(Trigger.isInsert){
        if(Trigger.isAfter){
            List<String> opttyIds = new List<String>();
            for(DonationSummary__c newdonsum : Trigger.new){                
                opttyIds.add(newdonsum.Opportunity__c);  
            }
            if(opttyIds.size() > 0){
                List<Opportunity> opptyRecs = [Select Id, Amount,EmailTriggered__c,Suppress_Receipt__c  From Opportunity Where Id  in : opttyIds];
                for(Opportunity oppty : opptyRecs){
                    AggregateResult[] groupedResults = [Select SUM(Total__c)amt From DonationSummary__c Where Opportunity__c = :oppty.Id];
                    oppty.Amount = (Decimal)groupedResults[0].get('amt');
                }
                try{
                    Update opptyRecs;  
                }catch(Exception ex){
                    System.debug(ex);
                }
            }
        }
    }
    if(Trigger.isUpdate){
        if(Trigger.isAfter){
            List<String> opttyIds = new List<String>();
            for(DonationSummary__c newdonsum : Trigger.new){                
                opttyIds.add(newdonsum.Opportunity__c);            
            }
            if(opttyIds.size() > 0){
                List<Opportunity> opptyRecs = [Select Id, Amount,EmailTriggered__c,Suppress_Receipt__c From Opportunity Where Id  in : opttyIds];
                for(Opportunity oppty : opptyRecs){
                    AggregateResult[] groupedResults = [Select SUM(Total__c)amt From DonationSummary__c Where Opportunity__c = :oppty.Id];
                    oppty.Amount = (Decimal)groupedResults[0].get('amt'); 
                }
                try{
                    Update opptyRecs;
                    
                }catch(Exception ex){
                    System.debug(ex);
                }
            }
        }
    }
    
}