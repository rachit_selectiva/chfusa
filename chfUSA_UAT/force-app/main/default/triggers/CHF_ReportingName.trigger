trigger CHF_ReportingName on Account (before update) {
    Account__c customSetting = Account__c.getOrgDefaults();
    if(customSetting.ReportingNameTrigger__c){
        if(Trigger.isUpdate){
            if(CHF_TriggerCheck.checkAccountUpdate){
                if(Trigger.isBefore){
                    CHF_TriggerCheck.checkAccountUpdate = false;
                    CHF_ReportingNameHandler reportHandler = new CHF_ReportingNameHandler();
                    reportHandler.checkingReportingNameHandler(Trigger.new,Trigger.oldMap);
                }
            }
        }
    }
}