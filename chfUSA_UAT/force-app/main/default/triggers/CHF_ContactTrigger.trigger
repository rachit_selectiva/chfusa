trigger CHF_ContactTrigger on Contact(after insert,after delete){
    Contact__c customSetting = Contact__c.getOrgDefaults();
    if(customSetting.contactTriggerHandler__c){
        if(Trigger.isInsert){
            if(Trigger.isAfter){
                if(CHF_TriggerCheck.checkContactInsert){
                    CHF_TriggerCheck.checkContactInsert = false;
                    CHF_ContactTriggerHandler cth = new CHF_ContactTriggerHandler();
                    cth.updatingAccount(Trigger.new);
                }
            }
        }
    }
}