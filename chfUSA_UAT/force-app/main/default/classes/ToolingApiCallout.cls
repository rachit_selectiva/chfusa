public class ToolingApiCallout {

    public static HttpResponse doGetCallout(String totalApiBody){
        HttpRequest request = new HttpRequest();
        String instanceUrl = URL.getOrgDomainUrl().toExternalForm();
        request.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());
        request.setHeader('Content-Type', 'application/json');
        request.setMethod('GET');
        request.setEndpoint(instanceUrl + '/services/data/v46.0/tooling/executeAnonymous/?anonymousBody=' + EncodingUtil.urlEncode(totalApiBody, 'UTF-8'));
        Http http = new Http();
        HttpResponse response = http.send(request);
        return response;
    }
    public static HttpResponse doPostCallout(String totalApiBody){
        HttpRequest request = new HttpRequest();
        String instanceUrl = URL.getOrgDomainUrl().toExternalForm();
        request.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());
        request.setHeader('Content-Type', 'application/json');
        request.setMethod('POST');
        request.setEndpoint(instanceUrl + '/services/data/v46.0/tooling/executeAnonymous/?anonymousBody=' + EncodingUtil.urlEncode(totalApiBody, 'UTF-8'));
        Http http = new Http();
        HttpResponse response = http.send(request);
        return response;
    }
}