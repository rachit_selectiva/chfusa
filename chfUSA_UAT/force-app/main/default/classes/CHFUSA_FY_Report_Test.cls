@isTest
public class CHFUSA_FY_Report_Test {
    
    
    public static Account getAccount(){
        Account accObj = new Account();
        accObj.Name = 'testAccount';        
        //accObj.Type = 'Household';        
        return accObj;
    }
    
    public static Opportunity getOpportunityObj(String accId){
        Opportunity opportunityObj = new Opportunity();
        opportunityObj.Name = 'testOpportunity';    
        opportunityObj.AccountId = accId;
        opportunityObj.CloseDate = Date.valueOf('2018-04-28');                
        opportunityObj.StageName= 'Payment Received';
        opportunityObj.Amount= 100;
        opportunityObj.Description = 'CHFUSA';
        opportunityObj.Transaction_ID__c = 'Check Trans';
        opportunityObj.Probability = 100; 
        opportunityObj.EmailTriggered__c = true; 
        //opportunityObj.FiscalYear= '2017';        
        return opportunityObj;
    }
    
    public static Contact getContactObj(String accId){
        Contact contactObj = new Contact();
        contactObj.FirstName = 'testFirst';
        contactObj.AccountId = accId;
        contactObj.Email = 'abc@gmail.com';
        contactObj.CHF_Account_Status__c = 'Approve';
        contactObj.Is_Email_Verify__c = true;
        contactObj.LastName = 'testLast';
        contactObj.Member_Account__c = false;       
        //opportunityObj.FiscalYear= '2017';        
        return contactObj;
    }
    
     public static DonationSummary__c getDomainSummary(String oppId){
        DonationSummary__c domainSum = new DonationSummary__c();
        domainSum.Campaign_Name__c = 'Sponsor-A-Student';
        domainSum.Remark__c = 'testRemark';
        domainSum.Amount__c = 500;
        domainSum.Quantity__c = 1;
        //domainSum.Total__c = 500;
        domainSum.Opportunity__c = oppId;
        return domainSum;
    }

      
    public static testMethod void testMethodCHFUSAPrint(){        
        Account accObj = TestCHFUSAReceiptDetailController.getAccount();
        insert accObj;
        System.debug('accObj : '+accObj);
        
        OpportunityReceipt__c settings5 = OpportunityReceipt__c.getOrgDefaults();
        settings5.CHF_MailID__c = 'rachit@selectiva.com,yogesh@selectiva.com';
        settings5.Send_Detail_Report__c = true;
        settings5.Send_Summary_Report__c = true;
        insert settings5;
        
        Account__c settings4 = Account__c.getOrgDefaults();
        settings4.checkingReportingNameHelper__c = true;
        settings4.ReportingNameTrigger__c = true;
        insert settings4;
        
        Contact__c settings3 = Contact__c.getOrgDefaults();
        settings3.contactTriggerHandler__c = true;
        settings3.updatingAccount__c = true;
        settings3.updatingAccountOnContactDelete__c = true;
        insert settings3;
        
         
        
        Opportunity oppObj = TestCHFUSAReceiptDetailController.getOpportunityObj(accObj.Id);
        try{
            insert oppObj ;
            System.debug('-test-oppObj--'+oppObj);
            System.debug('-test oppobj rec-'+[select Id, Name, Amount, FiscalYear,Serial_Number__c FROM Opportunity Where Account.Id =: accObj.Id]);
        }catch(Exception ex){
            System.debug(ex);
        }
        
        Contact conObj = TestCHFUSAReceiptDetailController.getContactObj(accObj.Id);
        insert conObj;
        System.debug('conObj : '+conObj);
                  
        DonationSummary__c domSumObj = TestCHFUSAReceiptDetailController.getDomainSummary(oppObj.Id);
        insert domSumObj;
        System.debug('domSumObj : '+domSumObj);    
        
        CHFUSA_FY_Report fyport = new CHFUSA_FY_Report();
        fyport.getCustomReport();
    }

}