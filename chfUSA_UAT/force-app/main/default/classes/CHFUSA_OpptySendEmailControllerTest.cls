@isTest(SeeAllData=false)
public class CHFUSA_OpptySendEmailControllerTest{
    
    public static OpportunityReceipt__c getOpportunityObj(){
        OpportunityReceipt__c opp = new OpportunityReceipt__c();
        opp.CHF_MailID__c = 'mukul.m@selectiva.com';
        opp.Send_Detail_Report__c = true;
        opp.Send_Summary_Report__c = true;
        insert opp; 
        return opp;
        
    }
    @IsTest
    static void callController(){
        OpportunityReceipt__c opp = CHFUSA_OpptySendEmailControllerTest.getOpportunityObj();
        PageReference myVfPage = Page.CHFUSA_OpptySendEmail;
        OpportunityReceipt__c opp1 =[select id,CHF_MailID__c,Send_Detail_Report__c,Send_Summary_Report__c from OpportunityReceipt__c where id=: opp.id];
        opp1.CHF_MailID__c = 'mukul.m@selectiva.com';
        opp1.Send_Detail_Report__c = false;
        opp1.Send_Summary_Report__c = true;
        update opp1;
        Account acc = CHF_TestData.createAccount('test11');
        Contact cont1 = CHF_TestData.createContact(acc, 'mukul11', 'test11');
        Opportunity testOpportunity1 = new Opportunity(
            Account = acc,
            Name = 'Test Opportunity Triggers',
            npsp__Primary_Contact__c = cont1.id,
            Transaction_ID__c='123465798',
            EmailTriggered__c = true,
            Amount=5000,
            StageName= 'Payment Received',
            CloseDate= system.today()+1,
            Donor__c  =cont1.id,
            Suppress_Receipt__c = false
            
        );
        insert testOpportunity1; 
        myVfPage.getParameters().put('id', String.valueOf(testOpportunity1.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(testOpportunity1);
        Test.setCurrentPage(myVfPage);
        Test.startTest();
        CHFUSA_OpptySendEmailController cont = new CHFUSA_OpptySendEmailController(sc);
        cont.sendEmail();
        Test.stopTest();
        
    }
    @IsTest
    static void callController1(){
        OpportunityReceipt__c opp = CHFUSA_OpptySendEmailControllerTest.getOpportunityObj();
        PageReference myVfPage = Page.CHFUSA_OpptySendEmail;
        OpportunityReceipt__c opp1 =[select id,CHF_MailID__c,Send_Detail_Report__c,Send_Summary_Report__c from OpportunityReceipt__c where id=: opp.id];
        opp1.CHF_MailID__c = 'mukul.m@selectiva.com';
        opp1.Send_Detail_Report__c = true;
        opp1.Send_Summary_Report__c = false;
        update opp1;
        Account acc = CHF_TestData.createAccount('test11');
        Contact cont1 = CHF_TestData.createContact(acc, 'mukul11', 'test11');
        Opportunity testOpportunity1 = new Opportunity(
            Account = acc,
            Name = 'Test Opportunity Triggers',
            npsp__Primary_Contact__c = cont1.id,
            Transaction_ID__c='123465798',
            EmailTriggered__c = true,
            Amount=5000,
            StageName= 'Payment Received',
            CloseDate= system.today()+1,
            Donor__c  =cont1.id,
            Suppress_Receipt__c = false
            
        );
        insert testOpportunity1;
        myVfPage.getParameters().put('id', String.valueOf(testOpportunity1.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(testOpportunity1);
        Test.setCurrentPage(myVfPage);
        Test.startTest();
        CHFUSA_OpptySendEmailController cont = new CHFUSA_OpptySendEmailController(sc);
        cont.sendEmail();
        Test.stopTest();   
    }
    
    @IsTest
    static void callController2(){
        OpportunityReceipt__c opp = CHFUSA_OpptySendEmailControllerTest.getOpportunityObj();
        PageReference myVfPage = Page.CHFUSA_OpptySendEmail;
        Account acc = CHF_TestData.createAccount('test11');
        Contact cont1 = CHF_TestData.createContact(acc, 'mukul11', 'test11');
        Opportunity testOpportunity1 = new Opportunity(
            Account = acc,
            Name = 'Test Opportunity Triggers',
            npsp__Primary_Contact__c = cont1.id,
            Transaction_ID__c='123465798',
            EmailTriggered__c = true,
            Amount=5000,
            StageName= 'Payment Received',
            CloseDate= system.today()+1,
            Donor__c  =cont1.id,
            Suppress_Receipt__c = false
            
        );
        insert testOpportunity1;
        myVfPage.getParameters().put('id', String.valueOf(testOpportunity1.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(testOpportunity1);
        Test.setCurrentPage(myVfPage);
        Test.startTest();
        CHFUSA_OpptySendEmailController cont = new CHFUSA_OpptySendEmailController(sc);
        cont.sendEmail();
        Test.stopTest();   
    }
    @IsTest
    static void callController3(){
        OpportunityReceipt__c opp = CHFUSA_OpptySendEmailControllerTest.getOpportunityObj();
        PageReference myVfPage = Page.CHFUSA_OpptySendEmail;
        OpportunityReceipt__c opp1 =[select id,CHF_MailID__c,Send_Detail_Report__c,Send_Summary_Report__c from OpportunityReceipt__c where id=: opp.id];
        opp1.CHF_MailID__c = 'mukul.m@selectiva.com';
        opp1.Send_Detail_Report__c = true;
        opp1.Send_Summary_Report__c = false;
        update opp1;
        Account acc = CHF_TestData.createAccount('test11');
        Contact cont1 = CHF_TestData.createContact(acc, 'mukul11', 'test11');
        Opportunity testOpportunity1 = new Opportunity(
            Account = acc,
            Name = 'Test Opportunity Triggers',
            npsp__Primary_Contact__c = cont1.id,
            Transaction_ID__c='123465798',
            EmailTriggered__c = true,
            Amount=5000,
            StageName= 'Payment Received',
            CloseDate= system.today()+1,
            Donor__c  = null,
            Suppress_Receipt__c = false
            
        );
        insert testOpportunity1;
        myVfPage.getParameters().put('id', String.valueOf(testOpportunity1.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(testOpportunity1);
        Test.setCurrentPage(myVfPage);
        Test.startTest();
        CHFUSA_OpptySendEmailController cont = new CHFUSA_OpptySendEmailController(sc);
        cont.sendEmail();
        Test.stopTest();    
    }
    @IsTest
    static void callController4(){
        OpportunityReceipt__c opp = CHFUSA_OpptySendEmailControllerTest.getOpportunityObj();
        PageReference myVfPage = Page.CHFUSA_OpptySendEmail;
        OpportunityReceipt__c opp1 =[select id,CHF_MailID__c,Send_Detail_Report__c,Send_Summary_Report__c from OpportunityReceipt__c where id=: opp.id];
        opp1.CHF_MailID__c = 'mukul.m@selectiva.com';
        opp1.Send_Detail_Report__c = true;
        opp1.Send_Summary_Report__c = false;
        update opp1;
        Account acc = CHF_TestData.createAccount('test11');
        Contact cont1 = CHF_TestData.createContact(acc, 'mukul11', 'test11');
        Contact cont2 = CHF_TestData.createContact(acc, 'mukul1', 'test1');
        Opportunity testOpportunity1 = new Opportunity(
            Account = acc,
            Name = 'Test Opportunity Triggers',
            npsp__Primary_Contact__c = cont1.id,
            Transaction_ID__c='123465798',
            EmailTriggered__c = true,
            Amount=5000,
            StageName= 'Payment Received',
            CloseDate= system.today()+1,
            Donor__c  = cont2.Id,
            Suppress_Receipt__c = false
            
        );
        insert testOpportunity1;
        myVfPage.getParameters().put('id', String.valueOf(testOpportunity1.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(testOpportunity1);
        Test.setCurrentPage(myVfPage);
        Test.startTest();
        CHFUSA_OpptySendEmailController cont = new CHFUSA_OpptySendEmailController(sc);
        cont.sendEmail();
        Test.stopTest();    
    }
    @IsTest
    static void differentCustom(){
        PageReference myVfPage = Page.CHFUSA_OpptySendEmail;
        Account acc = CHF_TestData.createAccount('test111');
        Contact cont1 = CHF_TestData.createContact(acc, 'mukul111', 'test111');
        Opportunity testOpportunity1 = new Opportunity(
            Account = acc,
            Name = 'Test Opportunity Triggers',
            npsp__Primary_Contact__c = cont1.id,
            Transaction_ID__c='123465798',
            EmailTriggered__c = true,
            Amount=5000,
            StageName= 'Payment Received',
            CloseDate= system.today()+1,
            Donor__c  =cont1.id,
            Suppress_Receipt__c = true
        );
        insert testOpportunity1;
        DonationSummary__c donationSummary = new DonationSummary__c();
        donationSummary.Campaign_Name__c = 'Special Projects';
        donationSummary.Quantity__c = 2;
        donationSummary.Amount__c = 300;
        donationSummary.Opportunity__c = testOpportunity1.Id;
        insert donationSummary;
        donationSummary.Amount__c = 400;
        update donationSummary;
        delete donationSummary;
        myVfPage.getParameters().put('id', String.valueOf(testOpportunity1.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(testOpportunity1);
        Test.setCurrentPage(myVfPage);
        Test.startTest();
        CHFUSA_OpptySendEmailController cont = new CHFUSA_OpptySendEmailController(sc);
        cont.sendEmail();
        Test.stopTest();
        
    }    
    @IsTest
    static void differentCustom1(){
        PageReference myVfPage = Page.CHFUSA_OpptySendEmail;
        Account acc = CHF_TestData.createAccount('test111');
        Contact cont1 = CHF_TestData.createContact(acc, 'mukul111', 'test111');
        Opportunity testOpportunity1 = new Opportunity(
            Account = acc,
            Name = 'Test Opportunity Triggers',
            npsp__Primary_Contact__c = cont1.id,
            Transaction_ID__c='123465798',
            EmailTriggered__c = true,
            Amount=5000,
            StageName= 'Payment Pending',
            CloseDate= system.today()+1,
            Donor__c  =cont1.id,
            Suppress_Receipt__c = true
        );
        insert testOpportunity1;
      
        DonationSummary__c donationSummary = new DonationSummary__c();
        donationSummary.Campaign_Name__c = 'Special Projects';
        donationSummary.Quantity__c = 2;
        donationSummary.Amount__c = 300;
        donationSummary.Opportunity__c = testOpportunity1.Id;
        insert donationSummary;
        donationSummary.Amount__c = 400;
        update donationSummary;
        delete donationSummary;
        myVfPage.getParameters().put('id', String.valueOf(testOpportunity1.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(testOpportunity1);
        Test.setCurrentPage(myVfPage);
        Test.startTest();
        CHFUSA_OpptySendEmailController cont = new CHFUSA_OpptySendEmailController(sc);
        cont.sendEmail();
        Test.stopTest();
        
    }      
}