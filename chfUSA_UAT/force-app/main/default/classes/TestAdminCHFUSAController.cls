@isTest
public class TestAdminCHFUSAController {
    
    public static Account getAccount(){        
        Account acc = new Account();
        acc.Name = 'Behind the Cloud';
        acc.Type = 'Household Account';
        acc.Phone = '9755000000';
        return acc;
    }
    public static DonationSummary__c getDonationSummary(String oppId){        
        DonationSummary__c donationSummary = new DonationSummary__c();
        donationSummary.Campaign_Name__c = 'Special Projects';
        donationSummary.Quantity__c = 2;
        donationSummary.Amount__c = 300;
        donationSummary.Opportunity__c = oppId;
        return donationSummary;
    }
    public static Opportunity getOpportunity(String accId){        
        Opportunity opp = new Opportunity();
        opp.CloseDate = Date.today();
        opp.Name = 'Test Opportunity';
        opp.Amount = 300;
        opp.StageName = 'Payment Received';
        opp.AccountId = accId;
        return opp;
    }
    
    //Validate AdminCHUSAController
    public static testMethod void validateDonationCustomReport() {
        Account acc = TestAdminCHFUSAController.getAccount();        
        insert acc;   
        System.assertNotEquals(acc.id, Null);
        
        Opportunity opp = TestAdminCHFUSAController.getOpportunity(acc.Id);
        insert opp;
        System.debug('opp : '+opp);
        
        DonationSummary__c donateSumm = TestAdminCHFUSAController.getDonationSummary(opp.Id);
        insert donateSumm;
        System.debug('donateSumm : '+donateSumm);
        
         Set<String> yearList;
        Test.startTest();
        
        
        AdminCHUSAController adminCHFCont = new AdminCHUSAController();
        yearList = adminCHFCont.yearList;
        ApexPages.currentPage().getParameters().put('firstParam', '2019');
        ApexPages.currentPage().getParameters().put('secondParam', '2019');
        adminCHFCont.getCustomReport();
         System.debug('adminCHFCont : '+adminCHFCont);
        Test.stopTest();
    }    
}