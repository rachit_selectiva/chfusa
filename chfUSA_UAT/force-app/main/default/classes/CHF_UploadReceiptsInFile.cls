public with sharing class CHF_UploadReceiptsInFile {
    Id accId;
   public void uploadFile(Blob detailReport, Blob summaryReport,String firstName,String lastName,String oppId){
        ContentVersion detail = new ContentVersion();
        ContentVersion summary = new ContentVersion();
        accId = [select id, Account.Id from Opportunity where Id =: oppId Limit 1].Account.Id;
        if(detailReport.size()>0 && summaryReport.size()>0){
            detail = createDetailReportContentVersion(detailReport,firstName,lastName);
            summary = createSummaryReportContentVersion(summaryReport,firstName,lastName);
            createDetailDocumentLink(detail);
            createSummaryDocumentLink(summary);
         }else if(detailReport.size()>0){
            detail = createDetailReportContentVersion(detailReport,firstName,lastName);
            Database.insert(detail);
            createDetailDocumentLink(detail);
         }else if(summaryReport.size()>0){
            summary = createSummaryReportContentVersion(summaryReport,firstName,lastName);
            Database.insert(summary);
            createSummaryDocumentLink(summary);
        }
    }

    public void createDetailDocumentLink(ContentVersion detail){
        Id detailDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:detail.Id].ContentDocumentId;
        ContentDocumentLink detailDocLink = new ContentDocumentLink();
        detailDocLink.ContentDocumentId = detailDoc;
        detailDocLink.LinkedEntityId = accId; // you can use objectId,GroupId etc
        detailDocLink.ShareType = 'V'; // Inferred permission, checkout description of ContentDocumentLink object for more details
        detailDocLink.Visibility = 'AllUsers';
        insert detailDocLink;
    }

    public void createSummaryDocumentLink(ContentVersion summary){
        Id summaryDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:summary.Id].ContentDocumentId;
        ContentDocumentLink summaryDocLink = new ContentDocumentLink();
        summaryDocLink.ContentDocumentId = summaryDoc;
        summaryDocLink.LinkedEntityId = accId; // you can use objectId,GroupId etc
        summaryDocLink.ShareType = 'V'; // Inferred permission, checkout description of ContentDocumentLink object for more details
        summaryDocLink.Visibility = 'AllUsers';
        insert summaryDocLink;
    }

    public ContentVersion createDetailReportContentVersion(Blob detailReportBlob,String firstName,String lastName){
        ContentVersion detail = new ContentVersion();
        detail.ContentLocation = 'S'; 
        detail.PathOnClient = firstName+'-'+lastName+'-'+Date.today().format().replace('/', '-')+'-Receipt.pdf';
        detail.Title = firstName+'-'+lastName+'-'+Date.today().format().replace('/', '-')+'-Receipt';
        detail.VersionData =detailReportBlob;
        insert detail;
        return detail;
    }

    public ContentVersion createSummaryReportContentVersion(Blob summaryReportBlob,String firstName,String lastName){
        ContentVersion summary = new ContentVersion();
        summary.ContentLocation = 'S'; 
        summary.PathOnClient = firstName+'-'+lastName+'-'+Date.today().format().replace('/', '-')+'-Summary-Receipt.pdf';
        summary.Title = firstName+'-'+lastName+'-'+Date.today().format().replace('/', '-')+'-Summary-Receipt';
        summary.VersionData = summaryReportBlob;
        insert summary;
        return summary;
    }
}