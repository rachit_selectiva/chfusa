@isTest(SeeAllData=false)
public class CHF_ReportingNameTestClass{
    
    @testSetup static void setup() {
        CHF_TestData.createCustomSetting();
        Account acc = CHF_TestData.createAccount('ShivSena');
        Contact cont1 = CHF_TestData.createContact(acc, 'Mahatma', 'Gandhi');
        Contact cont2 = CHF_TestData.createContact(acc, 'Putlibai', 'Gandhi');
        Contact cont3 = CHF_TestData.createContact(acc, 'Narendra', 'Gandhi');
        Contact cont4 = CHF_TestData.createContact(acc, 'Amit', 'Gandhi');
    }
    
    @isTest static void reportingNameDeleteContact() {    
        Contact cont1 = [Select id from Contact where firstName='Mahatma' limit 1];
        TEST.startTest();
        delete cont1;
        test.stopTest();
    }

    @isTest static void reportingNameDeleteContact1() {    
        Account acc = CHF_TestData.createAccount('BJP');
        Contact cont1 = CHF_TestData.createContact(acc, 'Narendra', 'Modi');
        Contact cont2 = CHF_TestData.createContact(acc, 'Chai', 'Modi');
        Contact cont3 = CHF_TestData.createContact(acc, 'Amit', 'Shah');
        Contact cont4 = CHF_TestData.createContact(acc, 'Kiran', 'Shah');
        TEST.startTest();
        delete cont1;
        test.stopTest();
    }
    static testMethod void reportingNameUpdateAccount(){
        Account acc = CHF_TestData.createAccountWithReportingName('Congress', 'Rahul');
        acc.Reporting_Name__c ='Testing';
        update acc;
    }
    // static testMethod void reportingNameTwoContact(){
    //     CHF_TestData.createCustomSetting();
    //     Account acc = CHF_TestData.createAccount('NCP');
    //     Contact cont1 = CHF_TestData.createContact(acc, 'Mahatma', 'Gandhi');
    //     Contact cont2 = CHF_TestData.createContact(acc, 'Putlibai', 'Gandhi');
    // }
    // static testMethod void reportingNameTwoContactWithDifferentName(){
    //     CHF_TestData.createCustomSetting();
    //     Account acc = CHF_TestData.createAccount('Congress');
    //     Contact cont1 = CHF_TestData.createContact(acc, 'Mahatma', 'Gandhi');
    //     Contact cont2 = CHF_TestData.createContact(acc, 'Narendra', 'Modi');
    //     // update acc;
    // }
    // static testMethod void reportingNameNContactWithDifferentName(){
    //     CHF_TestData.createCustomSetting();
    //     Account acc = CHF_TestData.createAccount('BJP');
    //     Contact cont1 = CHF_TestData.createContact(acc, 'Mahatma', 'Gandhi');
    //     Contact cont2 = CHF_TestData.createContact(acc, 'Narendra', 'Modi');
    //     Contact cont3 = CHF_TestData.createContact(acc, 'Amit', 'Modi');
    //     // update acc;
    // }
}