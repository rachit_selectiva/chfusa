public without sharing class CHF_ContactTriggerHelper{
    public void updatingAccount(List<Contact> insertedContacts){
        for(Contact ctn : insertedContacts){
            if(ctn.AccountId!=null){
                Account act = getAccount(ctn);
                if(!act.Manual_Reporting_Name__c){
                    String reportingName =  creatingReportingName(ctn);
                    act.Reporting_Name__c = reportingName;
                    act.ReportingNameByContact__c = false;
                    act.Manual_Reporting_Name__c = true;
                    try {
                        update act;
                    } catch (Exception ex) {
                        System.debug('error==>'+ex);
                    }
                }
            }
        }
    }
    public Account getAccount(Contact contact){
        return [select Id,Manual_Reporting_Name__c,ReportingNameByContact__c from Account where Id=:contact.AccountId];
    }
    public String creatingReportingName(Contact contact){
        return contact.LastName+', '+contact.FirstName;
    }
}