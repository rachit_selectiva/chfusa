public with sharing class CHFUSAReceiptDetailController {
    public String fisYr {set;get;}
    public List<Opportunity> opportunityList;    
    public Account acc;    
    
    public Account getAcc(){
        return acc;
    }
    
    public List<SelectOption> fiscalYearPick {
        get{
            
            try{
                AggregateResult[] fiscalYearList = [SELECT FiscalYear FROM Opportunity Group By FiscalYear];     
                fiscalYearPick = new List<SelectOption>();
                for (AggregateResult ar : fiscalYearList)  {     
                    system.debug('ficl'+ar.get('FiscalYear'));
                    fiscalYearPick.add(new SelectOption(String.valueOf(ar.get('FiscalYear')) ,String.valueOf(ar.get('FiscalYear'))));                
                    fiscalYearPick.sort();
                    //fiscalYearPick.add(new SelectOption('2018','2018'));
                }
                return fiscalYearPick;                 
            }catch(Exception ex){
                System.debug('ex- for fiscal'+ex);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'No Donation Summary is available for account.'));            
                return null;
            }                        
        }
        set;
    }
    
    
    private final Account record;
    public CHFUSAReceiptDetailController(ApexPages.StandardController controller) {  
        acc = new Account(); 
        this.record = (Account)controller.getRecord();
        //acc = (Account)controller.getRecord(); 
    }        
    static String body { get; set; }        
    Contact contactEmail;   
    
    public Contact getContactEmail() {   
        try{
            Contact contRec = [SELECT Id, Email,FirstName,LastName, Name FROM Contact WHERE Member_Relationship__c = '' and CHF_Account_Status__c = 'Approve' and Account.Id =: record.Id LIMIT 1];
            
            if(contRec.Email == null || contRec.Email == ''){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please update Email field in Contact.'));                
                return null;
            }else{
                return contRec;
            }
        }catch(Exception ex){
            System.debug('ex- for fiscal'+ex);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Contact information doesn\'t exists for this account or this contact is rejected.'));            
            return null;
        }
    }
    
    public PageReference printReceiptDetails() {
        Contact contRec = getContactEmail();
        System.debug('contRec'+contRec);
        if(contRec != Null){
            List<Opportunity> OpptyList = [SELECT Id From Opportunity where Account.Id = :record.id AND FiscalYear = :Integer.valueOf(fisYr)];
            Opportunity oppRec = [SELECT Id, Full_Quote_Name_Formula__c FROM Opportunity WHERE Account.Id =: record.Id LIMIT 1];
            PageReference p = new Pagereference('/apex/CHFUSAReceiptPrintPage?AccId=' + record.id + '&year=' + fisYr+ '&name=' + oppRec.Full_Quote_Name_Formula__c);
            p.setRedirect(true);        
            return p;            
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please update Email field in Contact.'));            
            return null;
        }       
    }
    
    @future(callout = true)
    public static void sendPDF(string conId, Integer currentFY,String contName,String firstName, String lastName,String accId ,List<String> emailAddress){
        String subEmail;
        EmailTemplate emailTemp = [select id,name,subject,htmlValue from emailtemplate where name = 'CHF_USA Thank You Template' limit 1];
        PageReference pdf = Page.CHFUSAReceiptPrintPage;
        pdf.getParameters().put('AccId',accId);
        pdf.getParameters().put('year',String.valueOf(currentFY));
        pdf.getParameters().put('name',contName);
        Blob attchBody;          
        try{
            attchBody = pdf.getContent();
        }catch(Exception ex){
            attchBody = Blob.valueOf('unit test body');
            System.debug('ex getContent- '+ex);
        }                
        Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
        msg.setTemplateId(emailTemp.Id);
        msg.setCcAddresses(emailAddress); 
        //msg.setBccAddresses(new List<String>{'milind@chfusa.org'});
        emailTemp.Subject = 'Donation Receipt for '+contName+' for FY '+currentFY;
        msg.setSubject('Donation Receipt for '+contName+' for FY '+currentFY);
        msg.setHtmlBody(emailTemp.htmlValue);
        msg.setTreatBodiesAsTemplate(true);
        
        msg.setTargetObjectId(conId);        
        Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
        attach.setContentType('application/pdf');
        attach.setFileName(firstName+'-'+lastName+'-'+currentFY+'-Receipt.pdf');
        attach.setInline(false);
        attach.Body = attchBody;
        Attachment accAttach = new Attachment();
        accAttach.Body = attchBody;
        accAttach.parentId = accId; 
        accAttach.ContentType = 'application/pdf';
        accAttach.Name = firstName+'-'+lastName+'-'+currentFY+'-Receipt.pdf';  
              
        ContentVersion cVersion = new ContentVersion();
        cVersion.ContentLocation = 'S';
        cVersion.PathOnClient = accAttach.Name;
        cVersion.Origin = 'H';
        cVersion.Title = accAttach.Name;
        cVersion.VersionData = accAttach.Body;
        try{
            insert cVersion;               
        }catch(Exception ex){
            System.debug('-ex-'+ex);
        }
        
        Id conDocument = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cVersion.Id].ContentDocumentId;
        ContentDocumentLink cDocLink = new ContentDocumentLink();
        cDocLink.ContentDocumentId = conDocument;
        cDocLink.LinkedEntityId = accAttach.ParentId;
        cDocLink.ShareType = 'V';
        cDocLink.Visibility = 'AllUsers';      
        try{
            insert cDocLink;               
        }catch(Exception ex){
            System.debug('-ex-'+ex);
        }
        
        msg.setFileAttachments(new Messaging.EmailFileAttachment[] { attach });
        msg.setSaveAsActivity(false);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { msg });
    } 
    
    public void sendReceiptDetails() {
        Contact contRec = getContactEmail();
        OpportunityReceipt__c receipt = OpportunityReceipt__c.getOrgDefaults();
        Opportunity oppRec = [SELECT Id, Full_Quote_Name_Formula__c FROM Opportunity WHERE Account.Id =: record.Id LIMIT 1];
        List<String> emailAddress = new List<String>();
        Integer optySize = 0;
        if(contRec != Null){
            emailAddress.add(contRec.Email);
            // emailAddress.add('milind@chfusa.org');
            // emailAddress.add('donations@chfusa.org');
            // emailAddress.add('contactus@chfusa.org');
            emailAddress.addAll(receipt.CHF_MailID__c.split(','));
            List<Opportunity> OpptyList = [SELECT Id From Opportunity where Account.Id = :record.id AND FiscalYear = :Integer.valueOf(fisYr)];
            optySize = OpptyList.size();
            
            if(OpptyList.size() <= 0){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'No donations were received for selected year - ' + fisYr + '. Please try again with a different year.'));            
            }else{                     
                sendPDF(String.valueOf(contRec.Id), Integer.valueOf(fisYr) ,oppRec.Full_Quote_Name_Formula__c,contRec.FirstName,contRec.LastName,String.valueOf(record.id),emailAddress);             
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Check your Email for Donation Receipt!'));
            } 
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please update Email field in Contact.'));            
        }        
    }    
}