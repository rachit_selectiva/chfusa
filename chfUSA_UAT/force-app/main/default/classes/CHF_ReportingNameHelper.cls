public without sharing class CHF_ReportingNameHelper {
    public void checkingReportingNameHelper(List<Account> updatedRecords,Map<Id,Account> oldRecords){
        List<Account> manualCheckRecords = new List<Account>();
        for(Account acc : updatedRecords){
            if(!String.isBlank(acc.Reporting_Name__c) && (acc.ReportingNameByContact__c)){
                String newReportingName = acc.Reporting_Name__c;
                String oldReportingName = oldRecords.get(acc.Id).Reporting_Name__c;
                if(!newReportingName.containsIgnoreCase(oldReportingName)){
                    acc.Manual_Reporting_Name__c = true;
                    acc.ReportingNameByContact__c= false;
                }
            }
        }
    }
}