public class CHFUSA_FY_Report {
    public String jsonDonSumm {get;set;}
    public PageReference getCustomReport() {        
        CHFUSA_YearVal__mdt[] value = [select Start_Date__c,End_Date__c FROM CHFUSA_YearVal__mdt];
        Date fromYear = value[0].Start_Date__c;
        Date toYear = value[0].End_Date__c;
        List<String> campaignNameList = new List<String>();
        campaignNameList.add('Special Projects');campaignNameList.add('Vantiga contributions');campaignNameList.add('Sponsor-A-Student');
        List<Account> accList = [SELECT ID, Name FROM Account WHERE RecordType.Name = 'Household Account'];
        List<AggregateResult> donSummList = new List<AggregateResult>();               
        for(String campName : campaignNameList){
            if(campName == 'Special Projects'){
                donSummList.addAll([SELECT SUM(Total__c) sp, Opportunity__r.AccountId, Opportunity__r.account.name, Opportunity__r.closeDate FROM DonationSummary__c WHERE Campaign_Name__c = 'Special Projects' and Opportunity__r.StageName = 'Payment Received' and Opportunity__r.AccountId in :accList and Opportunity__r.CloseDate >= :fromYear and Opportunity__r.CloseDate <= :toYear Group By Opportunity__r.AccountId, Opportunity__r.account.name, Opportunity__r.closeDate]);
            }if(campName == 'Vantiga contributions'){
                donSummList.addAll([SELECT SUM(Total__c) vc, Opportunity__r.AccountId, Opportunity__r.account.name, Opportunity__r.closeDate FROM DonationSummary__c WHERE Campaign_Name__c = 'Vantiga contributions' and Opportunity__r.StageName = 'Payment Received' and Opportunity__r.AccountId in :accList and Opportunity__r.CloseDate >= :fromYear and Opportunity__r.CloseDate <= :toYear Group By Opportunity__r.AccountId, Opportunity__r.account.name, Opportunity__r.closeDate]);
            }if(campName == 'Sponsor-A-Student'){
                donSummList.addAll([SELECT SUM(Total__c) sas, Opportunity__r.AccountId, Opportunity__r.account.name, Opportunity__r.closeDate FROM DonationSummary__c WHERE Campaign_Name__c = 'Sponsor-A-Student' and Opportunity__r.StageName = 'Payment Received' and Opportunity__r.AccountId in :accList and Opportunity__r.CloseDate >= :fromYear and Opportunity__r.CloseDate <= :toYear Group By Opportunity__r.AccountId, Opportunity__r.account.name, Opportunity__r.closeDate]);
            }
        }jsonDonSumm = JSON.serialize(donSummList);
        return null;
    }
}