public class CHFUSAPrintReceiptController {
    public Boolean isError {get;set;}
    public Account accRec {get;set;}       
    public String fiscalYear{set;get;}
    public String contName{set;get;}
    public String serialNumber{set;get;}
    public Decimal grandTotal{set;get;}
    public List<DonationSummary__c> donationSummaryLst{set;get;} 
    
    public CHFUSAPrintReceiptController() { 
        isError = false;
        String accountId;
        donationSummaryLst = new List<DonationSummary__c>();
        accountId = ApexPages.currentPage().getParameters().get('AccId');
        fiscalYear = ApexPages.currentPage().getParameters().get('year');
        contName = ApexPages.currentPage().getParameters().get('name');
        if(accountId != null && fiscalYear != null && contName != null){
            accRec = [Select Id, Name, Reporting_Name__c, BillingCity, BillingStreet, BillingState, BillingCountry, BillingPostalCode From Account Where Id = :accountId Limit 1];
            List<Opportunity> optyLst = [Select Id, Name, Amount, Serial_Number__c,FiscalYear,Probability FROM Opportunity Where Account.Id =: accountId AND Probability = 100 AND FiscalYear =: Integer.valueOf(fiscalYear) Order By Serial_Number__c];
            grandTotal = 0;
           	System.debug('optyLst'+optyLst);
            if(optyLst.size() > 0){
                serialNumber = optyLst[0].Serial_Number__c;
                for(Opportunity opty : optyLst){
                    grandTotal = grandTotal + opty.Amount;
                    donationSummaryLst.addAll([Select Id, Name, Donation_Date__c, Remark__c, Amount__c,Total__c, Quantity__c,Campaign_Name__c,Opportunity__c FROM DonationSummary__c WHERE Opportunity__c =: opty.Id]);    
                }
            }else{
                isError = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'No donations were received this year.'));
            }
            
        }else{
            isError = true;
            System.debug('optyLst');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'No donations were received this year.'));
        }        
    }
}