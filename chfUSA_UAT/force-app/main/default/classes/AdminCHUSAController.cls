public with sharing class AdminCHUSAController{
    public String jsonDonSumm {get;set;}
    public Integer currentYear {get{
        currentYear = datetime.now().year();
        return currentYear;
    }set;}
    
    public Set<String> yearList {
        get{
           AggregateResult[] totMonthList = [SELECT CALENDAR_YEAR(Opportunity__r.CloseDate) totYear FROM DonationSummary__c Group By Opportunity__r.CloseDate Order By CALENDAR_YEAR(Opportunity__r.CloseDate) ASC]; 
            yearList = new Set<String>();
            if(totMonthList != NULL){
                for(AggregateResult year : totMonthList){
                    if(year.get('totYear') != NULL)
                        yearList.add(String.valueOf(year.get('totYear')));
                }
            }
            return yearList;
        }set;
    } 
        
    public AdminCHUSAController(){
        List<String> campaignNameList = new List<String>();
        campaignNameList.add('Special Projects');campaignNameList.add('Vantiga contributions');campaignNameList.add('Sponsor-A-Student');campaignNameList.add('Kotekar Project');campaignNameList.add('Parijnanashram Vidyalaya');campaignNameList.add('Anandashraya');campaignNameList.add('Virar School');
        List<Account> accList = [SELECT ID, Name FROM Account WHERE RecordType.Name = 'Household Account'];
        List<AggregateResult> donSummList = new List<AggregateResult>();
        Integer currentYear = System.Today().year();
        for(String campName : campaignNameList){
            if(campName == 'Special Projects'){                
                donSummList.addAll([SELECT SUM(Total__c) sp, Opportunity__r.AccountId, Opportunity__r.npsp__Primary_Contact__r.Name FROM DonationSummary__c WHERE Campaign_Name__c = 'Special Projects' and Opportunity__r.StageName = 'Payment Received' and Opportunity__r.AccountId in :accList and CALENDAR_YEAR(Opportunity__r.CloseDate) = :currentYear Group By Opportunity__r.AccountId, Opportunity__r.npsp__Primary_Contact__r.Name]);
            }if(campName == 'Vantiga contributions'){
                donSummList.addAll([SELECT SUM(Total__c) vc, Opportunity__r.AccountId, Opportunity__r.npsp__Primary_Contact__r.Name FROM DonationSummary__c WHERE Campaign_Name__c = 'Vantiga contributions' and Opportunity__r.StageName = 'Payment Received' and Opportunity__r.AccountId in :accList and CALENDAR_YEAR(Opportunity__r.CloseDate) = :currentYear Group By Opportunity__r.AccountId, Opportunity__r.npsp__Primary_Contact__r.Name]);
            }if(campName == 'Sponsor-A-Student'){
                donSummList.addAll([SELECT SUM(Total__c) sas, Opportunity__r.AccountId, Opportunity__r.npsp__Primary_Contact__r.Name FROM DonationSummary__c WHERE Campaign_Name__c = 'Sponsor-A-Student' and Opportunity__r.StageName = 'Payment Received' and Opportunity__r.AccountId in :accList and CALENDAR_YEAR(Opportunity__r.CloseDate) = :currentYear Group By Opportunity__r.AccountId, Opportunity__r.npsp__Primary_Contact__r.Name]);
            }if(campName == 'Kotekar Project'){
                donSummList.addAll([SELECT SUM(Total__c) kot, Opportunity__r.AccountId, Opportunity__r.npsp__Primary_Contact__r.Name FROM DonationSummary__c WHERE Campaign_Name__c = 'Kotekar Project' and Opportunity__r.StageName = 'Payment Received' and Opportunity__r.AccountId in :accList and CALENDAR_YEAR(Opportunity__r.CloseDate) = :currentYear Group By Opportunity__r.AccountId, Opportunity__r.npsp__Primary_Contact__r.Name]);
            }if(campName == 'Parijnanashram Vidyalaya'){
                donSummList.addAll([SELECT SUM(Total__c) pari, Opportunity__r.AccountId, Opportunity__r.npsp__Primary_Contact__r.Name FROM DonationSummary__c WHERE Campaign_Name__c = 'Parijnanashram Vidyalaya' and Opportunity__r.StageName = 'Payment Received' and Opportunity__r.AccountId in :accList and CALENDAR_YEAR(Opportunity__r.CloseDate) = :currentYear Group By Opportunity__r.AccountId, Opportunity__r.npsp__Primary_Contact__r.Name]);
            }if(campName == 'Anandashraya'){
                donSummList.addAll([SELECT SUM(Total__c) ana, Opportunity__r.AccountId, Opportunity__r.npsp__Primary_Contact__r.Name FROM DonationSummary__c WHERE Campaign_Name__c = 'Anandashraya' and Opportunity__r.StageName = 'Payment Received' and Opportunity__r.AccountId in :accList and CALENDAR_YEAR(Opportunity__r.CloseDate) = :currentYear Group By Opportunity__r.AccountId, Opportunity__r.npsp__Primary_Contact__r.Name]);
            }if(campName == 'Virar School'){
                donSummList.addAll([SELECT SUM(Total__c) vir, Opportunity__r.AccountId, Opportunity__r.npsp__Primary_Contact__r.Name FROM DonationSummary__c WHERE Campaign_Name__c = 'Virar School' and Opportunity__r.StageName = 'Payment Received' and Opportunity__r.AccountId in :accList and CALENDAR_YEAR(Opportunity__r.CloseDate) = :currentYear Group By Opportunity__r.AccountId, Opportunity__r.npsp__Primary_Contact__r.Name]);
            }
        }jsonDonSumm = JSON.serialize(donSummList);
    }
    
    public PageReference getCustomReport() {
        string passedParam1 = Apexpages.currentPage().getParameters().get('firstParam');
        string passedParam2 = system.CurrentPageReference().getParameters().get('secondParam');        
        Integer fromYear = Integer.valueof(passedParam1.trim());
        Integer toYear = Integer.valueof(passedParam2.trim());        
        List<String> campaignNameList = new List<String>();
        campaignNameList.add('Special Projects');campaignNameList.add('Vantiga contributions');campaignNameList.add('Sponsor-A-Student');campaignNameList.add('Kotekar Project');campaignNameList.add('Parijnanashram Vidyalaya');campaignNameList.add('Anandashraya');campaignNameList.add('Virar School');
        List<Account> accList = [SELECT ID, Name FROM Account WHERE RecordType.Name = 'Household Account'];
        
        List<AggregateResult> donSummList = new List<AggregateResult>();               
        for(String campName : campaignNameList){
            if(campName == 'Special Projects'){                
                donSummList.addAll([SELECT SUM(Total__c) sp, Opportunity__r.AccountId, Opportunity__r.npsp__Primary_Contact__r.Name FROM DonationSummary__c WHERE Campaign_Name__c = 'Special Projects' and Opportunity__r.StageName = 'Payment Received' and Opportunity__r.AccountId in :accList and CALENDAR_YEAR(Opportunity__r.CloseDate) >= :fromYear and CALENDAR_YEAR(Opportunity__r.CloseDate) <= :toYear Group By Opportunity__r.AccountId, Opportunity__r.npsp__Primary_Contact__r.Name]);
            }if(campName == 'Vantiga contributions'){
                donSummList.addAll([SELECT SUM(Total__c) vc, Opportunity__r.AccountId, Opportunity__r.npsp__Primary_Contact__r.Name FROM DonationSummary__c WHERE Campaign_Name__c = 'Vantiga contributions' and Opportunity__r.StageName = 'Payment Received' and Opportunity__r.AccountId in :accList and CALENDAR_YEAR(Opportunity__r.CloseDate) >= :fromYear and CALENDAR_YEAR(Opportunity__r.CloseDate) <= :toYear Group By Opportunity__r.AccountId, Opportunity__r.npsp__Primary_Contact__r.Name]);
            }if(campName == 'Sponsor-A-Student'){
                donSummList.addAll([SELECT SUM(Total__c) sas, Opportunity__r.AccountId, Opportunity__r.npsp__Primary_Contact__r.Name FROM DonationSummary__c WHERE Campaign_Name__c = 'Sponsor-A-Student' and Opportunity__r.StageName = 'Payment Received' and Opportunity__r.AccountId in :accList and CALENDAR_YEAR(Opportunity__r.CloseDate) >= :fromYear and CALENDAR_YEAR(Opportunity__r.CloseDate) <= :toYear Group By Opportunity__r.AccountId, Opportunity__r.npsp__Primary_Contact__r.Name]);
            }if(campName == 'Kotekar Project'){
                donSummList.addAll([SELECT SUM(Total__c) kot, Opportunity__r.AccountId, Opportunity__r.npsp__Primary_Contact__r.Name FROM DonationSummary__c WHERE Campaign_Name__c = 'Kotekar Project' and Opportunity__r.StageName = 'Payment Received' and Opportunity__r.AccountId in :accList and CALENDAR_YEAR(Opportunity__r.CloseDate) >= :fromYear and CALENDAR_YEAR(Opportunity__r.CloseDate) <= :toYear Group By Opportunity__r.AccountId, Opportunity__r.npsp__Primary_Contact__r.Name]);
            }if(campName == 'Parijnanashram Vidyalaya'){
                donSummList.addAll([SELECT SUM(Total__c) pari, Opportunity__r.AccountId, Opportunity__r.npsp__Primary_Contact__r.Name FROM DonationSummary__c WHERE Campaign_Name__c = 'Parijnanashram Vidyalaya' and Opportunity__r.StageName = 'Payment Received' and Opportunity__r.AccountId in :accList and CALENDAR_YEAR(Opportunity__r.CloseDate) >= :fromYear and CALENDAR_YEAR(Opportunity__r.CloseDate) <= :toYear Group By Opportunity__r.AccountId, Opportunity__r.npsp__Primary_Contact__r.Name]);
            }if(campName == 'Anandashraya'){
                donSummList.addAll([SELECT SUM(Total__c) ana, Opportunity__r.AccountId, Opportunity__r.npsp__Primary_Contact__r.Name FROM DonationSummary__c WHERE Campaign_Name__c = 'Anandashraya' and Opportunity__r.StageName = 'Payment Received' and Opportunity__r.AccountId in :accList and CALENDAR_YEAR(Opportunity__r.CloseDate) >= :fromYear and CALENDAR_YEAR(Opportunity__r.CloseDate) <= :toYear Group By Opportunity__r.AccountId, Opportunity__r.npsp__Primary_Contact__r.Name]);
            }if(campName == 'Virar School'){
                donSummList.addAll([SELECT SUM(Total__c) vir, Opportunity__r.AccountId, Opportunity__r.npsp__Primary_Contact__r.Name FROM DonationSummary__c WHERE Campaign_Name__c = 'Virar School' and Opportunity__r.StageName = 'Payment Received' and Opportunity__r.AccountId in :accList and CALENDAR_YEAR(Opportunity__r.CloseDate) >= :fromYear and CALENDAR_YEAR(Opportunity__r.CloseDate) <= :toYear Group By Opportunity__r.AccountId, Opportunity__r.npsp__Primary_Contact__r.Name]);
            }            
        }jsonDonSumm = JSON.serialize(donSummList);
        return null;
    }
}