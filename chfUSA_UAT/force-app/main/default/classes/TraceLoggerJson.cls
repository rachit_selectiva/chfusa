public class TraceLoggerJson {

	public String TracedEntityId;
	public String ExpirationDate;
	public String DebugLevelId;
	public String LogType;
	public String StartDate;

	
	public static TraceLoggerJson parse(String json) {
		return (TraceLoggerJson) System.JSON.deserialize(json, TraceLoggerJson.class);
	}
}