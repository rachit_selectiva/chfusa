public class CHF_ContactTriggerHandler{
    Contact__c customSetting = Contact__c.getOrgDefaults();
    public void updatingAccount(List<Contact> insertedContacts){
        if (customSetting.updatingAccount__c) {
            CHF_ContactTriggerHelper cth = new CHF_ContactTriggerHelper();
            cth.updatingAccount(insertedContacts);
        }
    }
}