public class SendMail {
   @future(callout = true)
    public static void sendPDF(string conId, Integer currentFY,String firstName,String lastName,String oppId ,List<String> emailAddress){
        OpportunityReceipt__c receipt = OpportunityReceipt__c.getOrgDefaults();
        EmailTemplate emailTemp= [select id,name,subject,htmlValue from emailtemplate where name = 'CHF_USA Thank You Template' limit 1];
        PageReference detailReport;
        PageReference summaryReport;
        Blob detailReportBody;
        Blob summaryReportBody;
        if(receipt.Send_Detail_Report__c && receipt.Send_Summary_Report__c){
            detailReport = Page.CHFUSA_PdfGeneratorTemplate;
            detailReport.getParameters().put('id',oppId); 
            summaryReport = Page.CHFUSA_SingleDonationPDFGenerator;
            summaryReport.getParameters().put('id',oppId);
            try{
                detailReportBody = detailReport.getContent();
                summaryReportBody = summaryReport.getContent();
            }catch(Exception ex){
                detailReportBody = Blob.valueOf('unit test body');
                summaryReportBody = Blob.valueOf('unit test body');
                System.debug('ex getContent- '+ex);
            }    
        }else if(receipt.Send_Detail_Report__c){
            detailReport = Page.CHFUSA_PdfGeneratorTemplate;
            detailReport.getParameters().put('id',oppId);
            try{
                detailReportBody = detailReport.getContent();
            }catch(Exception ex){
                detailReportBody = Blob.valueOf('unit test body');
                System.debug('ex getContent- '+ex);
            }     
        }else if(receipt.Send_Summary_Report__c){
            summaryReport = Page.CHFUSA_SingleDonationPDFGenerator;
            summaryReport.getParameters().put('id',oppId);
            try{
                summaryReportBody = summaryReport.getContent();
            }catch(Exception ex){
                summaryReportBody = Blob.valueOf('unit test body');
                System.debug('ex getContent- '+ex);
            }    
        }
        Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
        String [] ccMails  = new List<String>();
        system.debug('conId--'+conId);
        msg.setTemplateId(emailTemp.Id);
        ccMails.addAll(receipt.CHF_MailID__c.split(','));
        if(emailAddress.size()==2){
            msg.setToAddresses(new String[]{emailAddress[1]});
            ccMails.add(emailAddress[0]);
            msg.setCcAddresses(ccMails);
        }else{
            msg.setToAddresses(emailAddress);
            msg.setCcAddresses(ccMails);
        }
        //msg.setBccAddresses(new List<String>{'milind@chfusa.org'});
        emailTemp.Subject = 'Donation Receipt for '+firstName+' '+lastName+' for FY '+currentFY;        
        msg.setSubject('Donation Receipt for '+firstName+' '+lastName+' for FY '+currentFY);
        msg.setHtmlBody(emailTemp.htmlValue);
        msg.setTreatBodiesAsTemplate(true);
        //msg.setTargetObjectId(UserInfo.getUserId());
        msg.setTargetObjectId(conId);        
        Messaging.EmailFileAttachment attachDetailReport;
        Messaging.EmailFileAttachment attachSummaryReport;
        if(receipt.Send_Detail_Report__c && receipt.Send_Summary_Report__c){
            attachDetailReport = new Messaging.EmailFileAttachment();
            attachSummaryReport = new Messaging.EmailFileAttachment();
            attachDetailReport.setContentType('application/pdf');        
            attachDetailReport.setFileName(firstName+'-'+lastName+'-'+currentFY+'-Receipt.pdf');
            attachDetailReport.setInline(false);
            attachDetailReport.Body = detailReportBody; 
            attachSummaryReport = new Messaging.EmailFileAttachment();
            attachSummaryReport.setContentType('application/pdf');        
            attachSummaryReport.setFileName(firstName+'-'+lastName+'-'+currentFY+'-Summary-Receipt.pdf');
            attachSummaryReport.setInline(false);
            attachSummaryReport.Body = summaryReportBody; 
            msg.setFileAttachments(new Messaging.EmailFileAttachment[] { attachDetailReport,attachSummaryReport });
        }else if(receipt.Send_Detail_Report__c ){
            attachDetailReport = new Messaging.EmailFileAttachment();
            attachDetailReport.setContentType('application/pdf');        
            attachDetailReport.setFileName(firstName+'-'+lastName+'-'+currentFY+'-Receipt.pdf');
            attachDetailReport.setInline(false);
            attachDetailReport.Body = detailReportBody; 
            msg.setFileAttachments(new Messaging.EmailFileAttachment[] { attachDetailReport });
        }else if(receipt.Send_Summary_Report__c){
            attachSummaryReport = new Messaging.EmailFileAttachment();
            attachSummaryReport.setContentType('application/pdf');        
            attachSummaryReport.setFileName(firstName+'-'+lastName+'-'+currentFY+'-Summary-Receipt.pdf');
            attachSummaryReport.setInline(false);
            attachSummaryReport.Body = summaryReportBody; 
            msg.setFileAttachments(new Messaging.EmailFileAttachment[] { attachSummaryReport });
        }
        msg.setSaveAsActivity(false);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { msg });
        CHF_UploadReceiptsInFile receiptFile = new CHF_UploadReceiptsInFile();
        try{
            receiptFile.uploadFile(detailReportBody,summaryReportBody,firstName,lastName,oppId);
        }catch(Exception ex){
            System.debug(ex);
        }
    } 
}