public with sharing class CHFUSA_PdfGeneratorTemplateController {
    
    public Object totalAmountForYear{get;set;}
    public Opportunity opp;
    public CHFUSA_PdfGeneratorTemplateController(ApexPages.StandardController controller){
        Id oppId = Apexpages.currentPage().getParameters().get('id');
        opp = [select id,name,Account.Id,CloseDate,Oppty_USA_Financial_year__c from Opportunity where id=:oppId];
        totalAmountForYear= calculateTotalAmountForYear(opp.Account.Id);
    }

    public Object calculateTotalAmountForYear(String accId){
        String Year =  opp.Oppty_USA_Financial_year__c;
        AggregateResult[] groupedResults  = [SELECT Account.Id, SUM(Amount)total FROM Opportunity where Account.Id=: accId  and Oppty_USA_Financial_year__c=:Year and StageName='Payment Received' group by Account.Id];
        return groupedResults[0].get('total');
    }
}