public class CHFUSA_OpptySendEmailController{
    public String toEmail{get;set;}
    public String ccEmail{get;set;}
    public Opportunity oppty;
    public CHFUSA_OpptySendEmailController(ApexPages.StandardController controller){
        Id oppId = Apexpages.currentPage().getParameters().get('id');
        OpportunityReceipt__c receipt = OpportunityReceipt__c.getOrgDefaults();
        this.oppty = getOpportunity(oppId);
        if(oppty.Donor__c!=Null){
            if(oppty.Donor__c!=oppty.npsp__Primary_Contact__c){
                this.toEmail = oppty.Donor__r.Email;
                this.ccEmail = receipt.CHF_MailID__c+', '+oppty.npsp__Primary_Contact__r.Email;
            }else{
                this.toEmail = oppty.npsp__Primary_Contact__r.Email;
                this.ccEmail = receipt.CHF_MailID__c;
            }
        }else{
            this.toEmail = oppty.npsp__Primary_Contact__r.Email;
            this.ccEmail = receipt.CHF_MailID__c;
        }
    }
    public Opportunity getOpportunity(Id oppId){
        Opportunity oppty = [select id,npsp__Primary_Contact__c,EmailTriggered__c,Suppress_Receipt__c,Transaction_ID__c,Donor__r.Email,npsp__Primary_Contact__r.Email,Mode_of_Payment__c,StageName from Opportunity where id=: oppId limit 1];
        return oppty;
    }
    public Boolean toggleEmailTrigger(Opportunity oppty){
        Boolean isSuccess = true;
        if(String.isNotBlank(oppty.Transaction_ID__c) && String.isNotBlank(oppty.Mode_of_Payment__c) && oppty.StageName.equals('Payment Received')){
            if(oppty.Suppress_Receipt__c == true){
                isSuccess = false;
                oppty.EmailTriggered__c = false;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please uncheck the Suppress Receipt checkbox for receiving the Email.'));
            }else{
                oppty.EmailTriggered__c =(oppty.EmailTriggered__c)?false:true;
                update oppty;
            }
        }
        if(oppty.StageName.equals('Payment Pending')){
            isSuccess = false;
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Opportunity payment is in Payment Pending status so receipt cannot be sent.'));
   }
        return isSuccess;
    }
    public Pagereference sendEmail(){
        Boolean isSuccess = toggleEmailTrigger(this.oppty);
        if(isSuccess){
            return new PageReference('javascript:window.close()');
        }
        return null;
        
    }
}