public class CHF_ReportingNameHandler {
    Account__c customSetting = Account__c.getOrgDefaults();
    public void checkingReportingNameHandler(List<Account> updatedRecords,Map<Id,Account> oldRecords){
        if(customSetting.checkingReportingNameHelper__c){
            CHF_ReportingNameHelper report = new CHF_ReportingNameHelper();
            report.checkingReportingNameHelper(updatedRecords,oldRecords);
        }
    }
}