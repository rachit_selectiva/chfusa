@isTest
public class TestCHFUSAReceiptDetailController {         
    
    public static Account getAccount(){
        Account accObj = new Account();
        accObj.Name = 'testAccount';        
        //accObj.Type = 'Household';        
        return accObj;
    }
    
    public static Opportunity getOpportunityObj(String accId){
        Opportunity opportunityObj = new Opportunity();
        opportunityObj.Name = 'testOpportunity';    
        opportunityObj.AccountId = accId;
        opportunityObj.CloseDate = Date.valueOf('2018-04-28');                
        opportunityObj.StageName= 'Payment Received';
        opportunityObj.Amount= 100;
        opportunityObj.Description = 'CHFUSA';
        opportunityObj.Transaction_ID__c = 'Check';
        opportunityObj.Probability = 100; 
        opportunityObj.EmailTriggered__c = true; 
        //opportunityObj.FiscalYear= '2017';        
        return opportunityObj;
    }
    
    public static Contact getContactObj(String accId){
        Contact contactObj = new Contact();
        contactObj.FirstName = 'testFirst';
        contactObj.AccountId = accId;
        contactObj.Email = 'abc@gmail.com';
        contactObj.CHF_Account_Status__c = 'Approve';
        contactObj.Is_Email_Verify__c = true;
        contactObj.LastName = 'testLast';
        contactObj.Member_Account__c = false;       
        //opportunityObj.FiscalYear= '2017';        
        return contactObj;
    }
    
    
    public static DonationSummary__c getDomainSummary(String oppId){
        DonationSummary__c domainSum = new DonationSummary__c();
        domainSum.Campaign_Name__c = 'Sponsor-A-Student';
        domainSum.Remark__c = 'testRemark';
        domainSum.Amount__c = 500;
        domainSum.Quantity__c = 1;
        //domainSum.Total__c = 500;
        domainSum.Opportunity__c = oppId;
        return domainSum;
    }
    
    public static testMethod void testMethodCHFUSAPrint(){        
        Account accObj = TestCHFUSAReceiptDetailController.getAccount();
        insert accObj;
        System.debug('accObj : '+accObj);
        
        OpportunityReceipt__c settings5 = OpportunityReceipt__c.getOrgDefaults();
        settings5.CHF_MailID__c = 'rachit@selectiva.com,yogesh@selectiva.com';
        settings5.Send_Detail_Report__c = true;
        settings5.Send_Summary_Report__c = true;
        insert settings5;
        
        Account__c settings4 = Account__c.getOrgDefaults();
        settings4.checkingReportingNameHelper__c = true;
        settings4.ReportingNameTrigger__c = true;
        insert settings4;
        
        Contact__c settings3 = Contact__c.getOrgDefaults();
        settings3.contactTriggerHandler__c = true;
        settings3.updatingAccount__c = true;
        settings3.updatingAccountOnContactDelete__c = true;
        insert settings3;
        
        Opportunity oppObj = TestCHFUSAReceiptDetailController.getOpportunityObj(accObj.Id);
        try{
            insert oppObj ;
            System.debug('-test-oppObj--'+oppObj);
        }catch(Exception ex){
            System.debug(ex);
        }
        
        Contact conObj = TestCHFUSAReceiptDetailController.getContactObj(accObj.Id);
        insert conObj;
        System.debug('conObj : '+conObj);
        
        DonationSummary__c domSumObj = TestCHFUSAReceiptDetailController.getDomainSummary(oppObj.Id);
        insert domSumObj;
        System.debug('domSumObj : '+domSumObj);        
        
        ApexPages.currentPage().getParameters().put('AccId', accObj.Id);
        ApexPages.currentPage().getParameters().put('year', '2018');
        ApexPages.currentPage().getParameters().put('name', 'testContact');        
        
        //Update Opportunity
        try{
            oppObj.Name = 'testOpportunityTest';
            update oppObj;            
        }catch(Exception ex){
            Boolean expectedExceptionThrown =  ex.getMessage().contains('Modification not allowed after payment received.') ? true : false;
            System.assertEquals(expectedExceptionThrown, true);
        }        
        
        try{
            domSumObj.Amount__c = 50;
            update domSumObj;            
        }catch(Exception ex){
            Boolean expectedExceptionThrown =  ex.getMessage().contains('Modification not allowed after payment received.') ? true : false;
            System.assertEquals(expectedExceptionThrown, true);
        }
        
        try{
            delete domSumObj;            
        }catch(Exception ex){
            Boolean expectedExceptionThrown =  ex.getMessage().contains('Modification not allowed after payment received.') ? true : false;
            System.assertEquals(expectedExceptionThrown, true);
        }
        
        try{
            oppObj.StageName = 'Payment Pending';
            update oppObj;      
        }catch(Exception ex){
            Boolean expectedExceptionThrown =  ex.getMessage().contains('Modification not allowed after payment received.') ? true : false;
            System.assertEquals(expectedExceptionThrown, true);
        }     
        
        try{
            domSumObj.Amount__c = 50;
            update domSumObj;      
        }catch(Exception ex){
            Boolean expectedExceptionThrown =  ex.getMessage().contains('Modification not allowed after payment received.') ? true : false;
            System.assertEquals(expectedExceptionThrown, true);
        }                      
        
        // Call constructor CHFUSAPrintReceiptController...
        CHFUSAPrintReceiptController chfUSAPrintCntrlloer = new CHFUSAPrintReceiptController();        
        
        // Call StandardController CHFUSAReceiptDetailController...                                 
        ApexPages.StandardController sc = new ApexPages.StandardController(accObj);
        CHFUSAReceiptDetailController chfUsaReceiptDetail = new CHFUSAReceiptDetailController(sc); 
        System.debug('-fiscalYear-'+chfUsaReceiptDetail.fiscalYearPick);
        
        // Call printReceiptDetails()
        chfUsaReceiptDetail.fisYr ='2018';
        System.debug(' opp fiscal year '+String.valueOf(oppObj.FiscalYear));
        //chfUsaReceiptDetail.fisYr = String.valueOf(oppObj.FiscalYear);        
        chfUsaReceiptDetail.printReceiptDetails();
        
        PageReference pageRef = Page.CHFUSAReceiptPrintPage;
        pageRef.setRedirect(true);        
        pageRef.getParameters().put('AccId', String.valueOf(accObj.Id));
        //pageRef.getParameters().put('year', String.valueOf(oppObj.FiscalYear));
        pageRef.getParameters().put('year', '2018');
		pageRef.getParameters().put('name', 'testContact');
        
        Blob content;                     
        Test.setCurrentPage(pageRef);
        
        // Call sendReceiptDetails
        chfUsaReceiptDetail.sendReceiptDetails();
        
        //Call sendPDF
        List<String> emailAddress = new List<String>();
        emailAddress.add(conObj.Email);    
        test.startTest();
        CHFUSAReceiptDetailController.sendPDF(conObj.Id, 2018, conObj.Name, conObj.FirstName, conObj.LastName, String.valueOf(accObj.Id), emailAddress);
        
        //Admin CHFUSA - AdminCHUSAController
        AdminCHUSAController adminCHF = new AdminCHUSAController();
        delete domSumObj;
        test.stopTest();
    }
}