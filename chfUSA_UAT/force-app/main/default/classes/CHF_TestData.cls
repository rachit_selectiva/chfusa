@isTest
public class CHF_TestData {
  
    public static void createCustomSetting(){
        Account__c account = new Account__c();
        account.ReportingNameTrigger__c= true;
        account.checkingReportingNameHelper__c= true;
        insert account;
        
        Contact__c contact = new Contact__c();
        contact.contactTriggerHandler__c = true;
        contact.updatingAccount__c = true;
        contact.updatingAccountOnContactDelete__c = true;
        insert contact;
    }
    public static Account createAccount(String name){
        Account acc = new Account();
        acc.Name=name;
        acc.ReportingNameByContact__c = true;
        insert acc;
        return acc;
    }
    public static Account createAccountWithReportingName(String name,String reportingName){
        Account acc = new Account();
        acc.Name=name;
        acc.ReportingNameByContact__c = true;
        acc.Reporting_Name__c=reportingName;
        insert acc;
        return acc;
    }
    public static Contact createContact(Account acc,String firstName, String lastName){
        Contact cont1 = new Contact();
        cont1.FirstName = firstName;
        cont1.LastName = lastName;
        cont1.Email = firstName+'@gmail.com';
        cont1.AccountId = acc.Id;
        insert cont1;

        return cont1;
    }

}